import numpy as np
import matplotlib.pyplot as plt

XOFFSET = +10 - 9.3 - 6

def readData(file,struct):
    with open(file,"r") as f:
        for line in f:
            if '#' in line: continue
            parse = line.strip().split()
            curr = float(parse[0])
            dist = float(parse[1]) + XOFFSET
            ang = int(parse[2])
            Bx = float(parse[3])
            By = float(parse[4])
            Bz = float(parse[5])

            if curr not in struct.keys():
                struct[curr] = {}       # create nested dict

            if dist not in struct[curr]:
                struct[curr][dist] = {}
                struct[curr][dist][ang] = [Bx,By,Bz]
            else:
                struct[curr][dist][ang] = [Bx,By,Bz]
                


def fixOffset(struct):
    x = []
    Bx = []
    By = []
    Bz = []
    x_ind = 0
    for dist in struct.keys():
        x.append(int(dist))# + OFFSET )
        Bx_off_up = 0
        Bx_off_do = 0
        By_off_up = 0
        By_off_do = 0
        Bz_off_up = 0
        Bz_off_do = 0
        for ang in struct[dist].keys():
            if ang == 0: 
                Bx_off_up = struct[dist][ang][0]
                By_off_up = struct[dist][ang][1]
                Bz_off_up = struct[dist][ang][2]

                Bx.append( struct[dist][ang][0] )
                By.append( struct[dist][ang][1] )
                Bz.append( struct[dist][ang][2] )

            if ang == 180: 
                Bx_off_do = struct[dist][ang][0]
                By_off_do = struct[dist][ang][1]
                Bz_off_do = struct[dist][ang][2]
        
        #Bx[x_ind] -= (Bx_off_up + Bx_off_do)/2.
        By[x_ind] -= (By_off_up + By_off_do)/2.
        Bz[x_ind] -= (Bz_off_up + Bz_off_do)/2.
        x_ind+=1
    

    x = np.asarray(x)
    Bz = np.asarray(Bz)/1e6
    By = np.asarray(By)/1e6
    Bx = np.asarray(Bx)/1e6

    inds = x.argsort()
    x = x[inds]
    Bz = Bz[inds]
    By = By[inds]
    Bx = Bx[inds]

    # Rotate the data based on bad orientation of fluxgate
    ang = -137*np.pi/180.
    By_fix = By * np.cos(ang) - Bz*np.sin(ang)
    Bz_fix = By * np.sin(ang) + Bz*np.cos(ang)

    return x,Bx,By_fix,Bz_fix

B_MT_1 = {}
B_MT_2 = {}
B_MB_1 = {}
B_OT = {}
B_OB = {}
B_OC = {}
B_CT = {}
B_CB = {}
B_IT_1 = {}
B_IT_2 = {}
B_IB_1 = {}
B_GC = {}
B_GC_R = {}
B_GC_T = {}
T_GC = {}
T_GC_R = {}
T_GC_T = {}
T_OC = {}
T_OT = {}
T_OB = {}
T_CT = {}
T_CB = {}
T_MT_1 = {}
T_MB_1 = {}
T_IT_1 = {}
T_IB_1 = {}
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-OC.txt",B_OC)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-OT.txt",B_OT)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-OB.txt",B_OB)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-MT-1.txt",B_MT_1)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-MT-2.txt",B_MT_2)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-MB-1.txt",B_MB_1)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-CT.txt",B_CT)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-CB.txt",B_CB)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-IT-1.txt",B_IT_1)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-IT-2.txt",B_IT_2)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-IB-1.txt",B_IB_1)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-GC.txt",B_GC)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-GC-R.txt",B_GC_R)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-GC-T.txt",B_GC_T)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_T-GC.txt",T_GC)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_T-GC-R.txt",T_GC_R)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_T-GC-T.txt",T_GC_T)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_T-OC.txt",T_OC)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_T-OT.txt",T_OT)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_T-OB.txt",T_OB)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_T-MT-1.txt",T_MT_1)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_T-MB-1.txt",T_MB_1)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_T-CT.txt",T_CT)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_T-CB.txt",T_CB)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_T-IT-1.txt",T_IT_1)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_T-IB-1.txt",T_IB_1)

data = [B_GC,B_GC_R,B_GC_T,B_OC,B_OT,B_OB,B_MT_1,B_MB_1,B_CT,B_CB,B_IT_1,B_IB_1,B_MT_2,B_IT_2,T_GC,T_GC_R,T_GC_T]
labels = ['Bottom coils (on-axis)','Bottom coils (right)','Bottom coils (up)','B-OC','B-OT','B-OB','B-MT-1','B-MB-1','B-CT','B-CB','B-IT-1','B-IB-1','B-MT-2','B-IT-2','Top coils (on-axis)','Top coils (right)','Top coils (up)']
nom_current = [1,1,1,100,300,300,120,120,80,80,11.2,11.2,1000,112.5,1,1,1]

#data = [T_OC]
#labels = ['T-OC']
#nom_current = [100]

colors = ['red','blue','green','orange','black','purple']
styles = ['-',':','--','-.','-','-']
x_sum = []
Bx_sum = [0,0,0,0]
By_sum = [0,0,0,0]
Bz_sum = [0,0,0,0]
for i in range(len(data)):

    x_nom,Bx_nom,By_nom,Bz_nom = fixOffset(data[i][nom_current[i]])
    x_inv,Bx_inv,By_inv,Bz_inv = fixOffset(data[i][-nom_current[i]])

    Bx_bac_2 = ( Bx_nom + Bx_inv ) /2.
    By_bac_2 = ( By_nom + By_inv ) /2.
    Bz_bac_2 = ( Bz_nom + Bz_inv ) /2.

    Bx_nom = Bx_nom - Bx_bac_2
    By_nom = By_nom - By_bac_2
    Bz_nom = Bz_nom - Bz_bac_2

    if 0 in data[i].keys():
        x_bac,Bx_bac,By_bac,Bz_bac = fixOffset(data[i][0])
        plt.figure(1)
        plt.plot(x_bac,Bx_bac,label='Background measured',color='blue',linestyle='none',marker='*',zorder=99)
        plt.figure(2)
        plt.plot(x_bac,By_bac,label='Background measured',color='blue',linestyle='none',marker='*',zorder=99)
        plt.figure(3)
        plt.plot(x_bac,Bz_bac,label='Background measured',color='blue',linestyle='none',marker='*',zorder=99)

    this_label = labels[i]+' at '+str(nom_current[i])+' (mA)'
    if nom_current[i] == 1:
        this_label = labels[i]

    plt.figure(1)
    plt.plot(x_nom,Bx_bac_2,label='Background estimated',color='black',linestyle='--',marker='s',alpha=0.5)
    plt.plot(x_nom,Bx_nom,label=this_label,color='red',linestyle='--',marker='o')
    plt.axhline(y=0,color='black',linestyle='--')
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xlabel('x (distance from 1st $\mu$-metal layer)',fontsize=17)
    plt.ylabel(r'$B_x$ ($\mu$T, arb. offset)',fontsize=17)
    plt.legend(numpoints=1,loc='best')
    plt.tight_layout()
    plt.savefig(labels[i]+'-Bx.png',bbox_inches='tight')
    plt.close(1)

    plt.figure(2)
    plt.plot(x_nom,By_bac_2,label='Background estimated',color='black',linestyle='--',marker='s',alpha=0.5)
    plt.plot(x_nom,By_nom,label=this_label,color='red',linestyle='--',marker='o')
    plt.axhline(y=0,color='black',linestyle='--')
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xlabel('x (distance from 1st $\mu$-metal layer)',fontsize=17)
    plt.ylabel(r'$B_y$ ($\mu$T)',fontsize=17)
    plt.legend(numpoints=1,loc='best')
    plt.tight_layout()
    plt.savefig(labels[i]+'-By.png',bbox_inches='tight')
    plt.close(2)

    plt.figure(3)
    plt.plot(x_nom,Bz_bac_2,label='Background estimated',color='black',linestyle='--',marker='s',alpha=0.5)
    plt.plot(x_nom,Bz_nom,label=this_label,color='red',linestyle='--',marker='o')
    plt.axhline(y=0,color='black',linestyle='--')
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xlabel('x (distance from 1st $\mu$-metal layer)',fontsize=17)
    plt.ylabel(r'$B_z$ ($\mu$T)',fontsize=17)
    plt.legend(numpoints=1,loc='best')
    plt.tight_layout()
    plt.savefig(labels[i]+'-Bz.png',bbox_inches='tight')
    plt.close(3)





plt.show()



