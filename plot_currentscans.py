import numpy as np
import matplotlib.pyplot as plt

def readData(file,struct):
    with open(file,"r") as f:
        for line in f:
            if '#' in line: continue
            parse = line.strip().split()
            curr = float(parse[0])
            dist = float(parse[1])
            ang = int(parse[2])
            Bx = float(parse[3])
            By = float(parse[4])
            Bz = float(parse[5])

            if curr not in struct.keys():
                struct[curr] = {}       # create nested dict

            if dist not in struct[curr]:
                struct[curr][dist] = {}
                struct[curr][dist][ang] = [Bx,By,Bz]
            else:
                struct[curr][dist][ang] = [Bx,By,Bz]
                


def fixOffset(struct):
    x = []
    Bx = []
    By = []
    Bz = []
    x_ind = 0
    for dist in struct.keys():
        x.append(int(dist))# + OFFSET )
        Bx_off_up = 0
        Bx_off_do = 0
        By_off_up = 0
        By_off_do = 0
        Bz_off_up = 0
        Bz_off_do = 0
        for ang in struct[dist].keys():
            if ang == 0: 
                Bx_off_up = struct[dist][ang][0]
                By_off_up = struct[dist][ang][1]
                Bz_off_up = struct[dist][ang][2]

                Bx.append( struct[dist][ang][0] )
                By.append( struct[dist][ang][1] )
                Bz.append( struct[dist][ang][2] )

            if ang == 180: 
                Bx_off_do = struct[dist][ang][0]
                By_off_do = struct[dist][ang][1]
                Bz_off_do = struct[dist][ang][2]
        
        Bx[x_ind] -= (Bx_off_up + Bx_off_do)/2.
        By[x_ind] -= (By_off_up + By_off_do)/2.
        Bz[x_ind] -= (Bz_off_up + Bz_off_do)/2.
        x_ind+=1
    

    x = np.asarray(x)
    Bz = np.asarray(Bz)/1e6
    By = np.asarray(By)/1e6
    Bx = np.asarray(Bx)/1e6

    inds = x.argsort()
    x = x[inds]
    Bz = Bz[inds]
    By = By[inds]
    Bx = Bx[inds]

    # Rotate the data based on bad orientation of fluxgate
    ang = -137*np.pi/180.
    By_fix = By * np.cos(ang) - Bz*np.sin(ang)
    Bz_fix = By * np.sin(ang) + Bz*np.cos(ang)

    return x,Bx,By_fix,Bz_fix

B_MT_1 = {}
B_MT_2 = {}
B_MB_1 = {}
B_OT = {}
B_OB = {}
B_OC = {}
B_CT = {}
B_CB = {}
B_IT_1 = {}
B_IT_2 = {}
B_IB_1 = {}
B_GC = {}
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-OC.txt",B_OC)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-OT.txt",B_OT)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-OB.txt",B_OB)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-MT-1.txt",B_MT_1)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-MT-2.txt",B_MT_2)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-MB-1.txt",B_MB_1)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-CT.txt",B_CT)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-CB.txt",B_CB)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-IT-1.txt",B_IT_1)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-IT-2.txt",B_IT_2)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-IB-1.txt",B_IB_1)
readData("/Users/efrainsegarra/work/n2edm/guidecoils/read_fluxgate/execs/v2_B-GC.txt",B_GC)

data = [B_GC]
labels = ['B-GC']
curr_scan_pos = [80]



colors = ['red','blue','green','orange','black','purple']
styles = ['-',':','--','-.','-','-']
x_sum = []
Bx_sum = [0,0,0,0]
By_sum = [0,0,0,0]
Bz_sum = [0,0,0,0]
for i in range(len(data)):
    
    Curr = []
    Bx = []
    By = []
    Bz = []
    Bx_bac = 0
    By_bac = 0
    Bz_bac = 0
    for curr in data[i].keys():
        x,bx,by,bz = fixOffset(data[i][curr])
        if len(np.where(x==curr_scan_pos[i])[0]) == 0: continue
        scan_index = (np.where(x == curr_scan_pos[i]))[0][0]
        Curr.append(curr)
        Bx.append(bx[scan_index])
        By.append(by[scan_index])
        Bz.append(bz[scan_index])
        if curr == 0:
            Bx_bac = bx[scan_index]
            By_bac = by[scan_index]
            Bz_bac = bz[scan_index]

    Curr = np.asarray(Curr)
    Bx = np.asarray(Bx) - Bx_bac
    By = np.asarray(By) - By_bac
    Bz = np.asarray(Bz) - Bz_bac

    inds = Curr.argsort()
    Curr = Curr[inds]
    Bz = Bz[inds]
    By = By[inds]
    Bx = Bx[inds]



    plt.figure(1)
    plt.plot(Curr,Bx,label=labels[i],color=colors[i],linestyle=styles[i],marker='o')
    
    plt.figure(2)
    plt.plot(Curr,By,label=labels[i],color=colors[i],linestyle=styles[i],marker='o')
    
    plt.figure(3)
    plt.plot(Curr,Bz,label=labels[i],color=colors[i],linestyle=styles[i],marker='o')


plt.figure(1)
#plt.plot(x_sum,Bx_sum,color='black',linewidth=3)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.xlabel('I (mA)',fontsize=17)
plt.ylabel(r'$B_x$ ($\mu$T, arb. offset)',fontsize=17)
plt.legend(numpoints=1,loc='best')
plt.tight_layout()
plt.savefig('B-MT-2-Bx_currentscan.png',bbox_inches='tight')

plt.figure(2)
#plt.plot(x_sum,By_sum,color='black',linewidth=3)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.xlabel('I (mA)',fontsize=17)
plt.ylabel(r'$B_y$ ($\mu$T)',fontsize=17)
plt.legend(numpoints=1,loc='best')
plt.tight_layout()
plt.savefig('B-MT-2-By_currentscan.png',bbox_inches='tight')

plt.figure(3)
#plt.plot(x_sum,Bz_sum,color='black',linewidth=3)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.xlabel('I (mA)',fontsize=17)
plt.ylabel(r'$B_z$ ($\mu$T)',fontsize=17)
plt.legend(numpoints=1,loc='best')
plt.tight_layout()
plt.savefig('B-MT-2-Bz_currentscan.png',bbox_inches='tight')

plt.show()



